import Vue from 'vue'
import index from './index.vue'
import Vant from 'vant';
import 'vant/lib/index.css';
import Vuex from 'vuex'
import vueEsign from 'vue-esign'
import http from '../src/js/http.js'  
// import router from './js/router.js' 
 


Vue.prototype.$http = http;
Vue.use(vueEsign)
Vue.use(Vuex);
Vue.use(Vant); 
const store = new Vuex.Store();

Vue.config.productionTip = false

new Vue({
	el:"#app",
	store,
	// router,
	render: h => h(index)
	// PDFJS
})
