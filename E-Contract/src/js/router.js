import Vue from 'vue'
import Router from 'vue-router'


import test from '../../src/test.vue'


Vue.use(Router)

var router= new Router({
  routes: [
    {
      path: '/src/test2',
      name: 'test2',
      component: test2
    },
    {
      path: '/src/test',
      name: 'test',
      component: test
    },

  ]
})

export default router;
// const originalReplace = Router.prototype.replace;
// Router.prototype.replace = function replace(location) {
//     return originalReplace.call(this, location).catch(err => err);
// };