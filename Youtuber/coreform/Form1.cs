﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace coreform
{
    public partial class Form1 : Form
    {
        public Process process = null;
        public Form1()
        {
            this.FormClosed += Form1_FormClosed;
            InitializeComponent();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (process != null)
            {
                Process[] processes = Process.GetProcessesByName("wbCore");
                foreach (Process p in processes)
                    p.Kill();
                //process.Close();
                Process.GetCurrentProcess().Kill();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.webView21.Source = new Uri("http://127.0.0.1:1142");
        }

    }
}
