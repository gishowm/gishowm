using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace coreform
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var exer = ExeCuteHost();
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var fm = new Form1();
            fm.process = exer;
            Application.Run(fm);


        }


        static Process ExeCuteHost()
        {
            var proc = new Process();
            var baseUrl = Environment.CurrentDirectory.ToString();
            proc.StartInfo.FileName = @$"{baseUrl}/web/wbCore.exe";
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.CreateNoWindow = false;
            proc.Start();
            var sw = proc.StandardInput;
            return proc;
        }


    }
}
